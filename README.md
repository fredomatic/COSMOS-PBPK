Physiologically-based pharmacokinetic models developed during the European COSMOS projet.
For documentation see:

- Bois F., Diaz Ochoa J.G. Gajewska M., Kovarich S., Mauch K., Paini A., Péry A., Sala Benito J.V., Teng S., Worth A., 2017, 
  Multiscale modelling approaches for assessing cosmetic ingredients safety, Toxicology, 392:130-139, doi:10.1016/j.tox.2016.05.026.

- Berggren E., White A., Ouedraogo G., Paini A., Richarz A.-N., Bois F., Exner T., Leite S., van Grunsven L.A., Worth A., Mahony C., 2017, 
  Ab initio chemical safety assessment: a workflow based on exposure considerations and non-animal methods, Computational Toxicology, 4:31-44,
  doi: 10.1016/j.comtox.2017.10.001.

The model has been subsequently improved and extended within the framework of the EuroMix project (European Test and Risk Assessment Strategies for Mixtures
Grant Agreement number 633172 – Collaborative project H2020-SFS-2014-2). The driving and preprocessing R scripts are not kept in sync with the model, because 
they are not used in EuroMix.
